# Endpoints

---

## GET /sunspots
Retrieve the entire dataset

- Expected JSON response:
```
[
    {'id': 0, 'sunspots': 101, 'year': 1770},
    {'id': 1, 'sunspots': 82, 'year': 1771},
    {'id': 2, 'sunspots': 67, 'year': 1772},
    {'id': 3, 'sunspots': 35, 'year': 1773},
    {'id': 4, 'sunspots': 31, 'year': 1774}
        .
        .
        .
    {'id': 97, 'sunspots': 7, 'year': 1867},
    {'id': 98, 'sunspots': 38, 'year': 1868},
    {'id': 99, 'sunspots': 74, 'year': 1869}
]
```

## GET /sunspots/year/<year>
Retrieve the number of sunspots in a given year

- Parameters:

Valid year entries consist of integers from 1770 to 1869.

- Expected JSON response:

```
{'id': <id>, 'sunspots': <sunspots>, 'year': <year>}
```
where `<id>` and `<sunspots>` are the corresponding values for the specified year.


## POST /sunspots/year/<year>
Update the number of sunspots in a given year

- Expected JSON response:

```
{'status_code': 200}
```

## GET /sunspots/id/<id>
Retrieve the number of sunspots by its ID

- Expected JSON response:

```
{'id': <id>, 'sunspots': <sunspots>, 'year': <year>}
```
where `<sunspots>` and `<year>` are the corresponding values for the specified year.

## POST /sunspots/id/<id>
Update the number of sunspots by its ID

- Expected JSON response:

```
{'status_code': 200}
```

## GET /sunspots?start=<start>&end=<end>
Retrieve the number of sunspots from <start> to <end>

- Parameters:
One or both parameters may be entered, as long as start > end. Both should be integers between 1770 and 1869.
If only one parameter is specified, the other defaults to the first or last year in the dataset.

- Expected JSON response:

```
[
    {'id': <start_id>, 'sunspots': <start_sunspots>, 'year': <start>},
        .
        .
        .
    {'id': <end_id>, 'sunspots': <end_sunspots>, 'year': <end>}
]
```
where `<start_id>`, `<start_sunspots>`, `<end_id>`, and `<end_sunspots>` are the values corresponding to each year in the provided range.

## GET /sunspots?limit=<limit>&offset=<offset>
Retrieve <limit> sunspots in order, offset from the first datapoint by <offset> years

- Parameters:
One or both parameters may be entered. Both parameters should be integers between 1 and 100.
If `offset` is not specified, it will be set to zero, and if `limit` is not specified, the entire remaining dataset after `offset` will be returned.

- Expected JSON response:

```
[
    {'id': <offset>, 'sunspots': <offset_sunspots>, 'year': <offset_year>},
        .
        .
        .
    {'id': <limit+offset>, 'sunspots': <limit_sunspots>, 'year': <limit_year>}
]
```
where `<offset_id>`, `<offset_sunspots>`, and `<offset_year>` are the corresponding values with `id` equal to `offset`. Then follow `limit` additional entries, in order, unless the end of the dataset is reached.

## GET /sunspots/mean?start=<start>&end=<end>
Retrieve the mean number of sunspots in the database

- Parameters:
One or both parameters may be entered, as long as start > end. Both should be integers between 1770 and 1869.
If only one parameter is specified, the other defaults to the first or last year in the dataset.

- Expected JSON response:

```
{'mean': <mean>}
```
where `<mean>` is the computed mean of the specified data.

## GET /sunspots/graph
Retrieve a graph of the number of sunspots over time

- Expected JSON response:

```
{'status_code': 200}
```

---

# Examples

```
$ curl "http://localhost:5000/sunspots?limit=5&offset=30"
[
  {
    "id": 30, 
    "sunspots": 15, 
    "year": 1800
  }, 
  {
    "id": 31, 
    "sunspots": 34, 
    "year": 1801
  }, 
  {
    "id": 32, 
    "sunspots": 45, 
    "year": 1802
  }, 
  {
    "id": 33, 
    "sunspots": 43, 
    "year": 1803
  }, 
  {
    "id": 34, 
    "sunspots": 48, 
    "year": 1804
  }
]
```

```
$ python3

>>> import requests
>>> rsp = requests.get('http://localhost:5000/sunspots?start=1800&end=1810')
>>> rsp.json()
[{'id': 30, 'sunspots': 15, 'year': 1800}, {'id': 31, 'sunspots': 34, 'year': 1801}, {'id': 32, 'sunspots': 45, 'year': 1802}, {'id': 33, 'sunspots': 43, 'year': 1803}, {'id': 34, 'sunspots': 48, 'year': 1804}, {'id': 35, 'sunspots': 42, 'year': 1805}, {'id': 36, 'sunspots': 28, 'year': 1806}, {'id': 37, 'sunspots': 10, 'year': 1807}, {'id': 38, 'sunspots': 8, 'year': 1808}, {'id': 39, 'sunspots': 3, 'year': 1809}, {'id': 40, 'sunspots': 0, 'year': 1810}]
```


---

# Billing

To help support us in maintaining the resourses required to run this API, each request will be charged based on the amount of CPU time required. 

Because of the unforeseen and overwhelming interest in our project and our limited resources, clients will be limited to 10,000 requests per day.
